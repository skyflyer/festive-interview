# The Festive Interview

This project contains *2* assignments, each of which contains *2* parts.

You can solve each part and each assignment using any programming language/technology you like (we suggest C#, but you may use Python, JavaScript, etc.)

You can use any resources you prefer (websites, StackOverflow, debuggers, unit tests, your favourite code editor, etc.)

**Before starting**, create a new `git` branch with your name

After completing each part of each assignment, commit the solution and push it to the remote branch
